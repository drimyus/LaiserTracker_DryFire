# README #

Raiser tracking for dry fire game

### Specification ###

* UI with PyQt
* Multi thread
* Sound effect
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Implementation ###

Tracking the raiser on different lightness condition.
If detect the raiser light, the notice with sound
For multi-target tracking, multi threads was used.
UI was implemented by using PyQt